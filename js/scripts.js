

var arrayContacts = [];
var arrayFavContacts = [];

defaultContact(arrayContacts);



function localStorage() {
	// local storage
	var JSONreadyUsers = JSON.stringify(arrayContacts);

	localStorage.setItem('arrayContacts', JSONreadyUsers);
	console.log(JSON.parse(localStorage.arrayContacts));
}

//ajout contact a la liste des contacts
document.getElementById('ajouter').onclick = function () {
	
	cleanErrors();
	//format d'email
	var formatEmail = /^([0-9a-zA-Z]([-_\\.]*[0-9a-zA-Z]+)*)@([0-9a-zA-Z]([-_\\.]*[0-9a-zA-Z]+)*)[\\.]([a-zA-Z]{2,9})$/;
	
	var nom = document.getElementById('nom').value;
	var prenom = document.getElementById('prenom').value;
	var telephone = document.getElementById('telephone').value;
	var email = document.getElementById('email').value;
	var erreur = 0;
	
	if(nom === "") {
		document.getElementById('erreurNom').style.display="block";
		erreur = 1;
	}
	
	if(prenom === "") {
		document.getElementById('erreurPrenom').style.display="block";
		erreur = 1;
	}
	
	if(email === "") {
		document.getElementById('erreurEmail').style.display="block";
		erreur = 1;
	}else{
		if(!formatEmail.test(email)){
			document.getElementById('erreurValidEmail').style.display="block";
			erreur = 1;
		}else{
			document.getElementById('erreurValidEmail').style.display="none";
		}
	}
	
	if(telephone === "") {
		document.getElementById('erreurTel').style.display="block";
		erreur = 1;
	}else{
		if(isNaN(telephone)){
			document.getElementById('erreurNumTel').style.display="block";
			erreur=1;
		}else{
			document.getElementById('erreurNumTel').style.display="none";
			if (telephone.length!=10)
			{
				document.getElementById('erreurChiffre').style.display="block";
				erreur=1;
			}else{
				document.getElementById('erreurChiffre').style.display="none";
			}
		}
	}
	
	if(erreur===0){
		var cnt = {
				kid : arrayContacts.length + 1,
				knom: nom,
				kprenom: prenom,
				ktel: telephone,
				kemail: email
			};
		arrayContacts.push(cnt);
		clearInputs();
		//showSuccessMessage();
		document.getElementById('msgSuccess').innerHTML = "Le contact a été bien ajouté";
		// On l'efface 8 secondes plus tard
		setTimeout(function() {
		  document.getElementById('msgSuccess').innerHTML = "";
		},2000);
	
	}
	return false;
};


//linkAjoutContact
document.getElementById('linkAjoutContact').onclick = function(){
	panelActive(1);
	showPanel(1);
	return false;
};
//linkContact
document.getElementById('linkContact').onclick = function(){
	panelActive(2);
	refrechList();
	showPanel(2);

	return false;
};



//link favoris 
document.getElementById('linkFav').onclick = function(){
	panelActive(3);
	//cacher le bloc favoris tant qu'il est vide
	if(arrayFavContacts.length){
		document.getElementById('aucunFav').innerHTML="";
		document.getElementsByTagName('thead')[1].style.display="";
		showPanel(3);

	}else{
		document.getElementById('favoris').style.display="block";
		document.getElementById('contacts').style.display="none";
		document.getElementsByTagName('thead')[1].style.display="none";

		document.getElementById('aucunFav').innerHTML="Aucun contact n'est ajouté a la section favoris";
	}
	
	//lignes du tableau
	var row = "";
	var tbody = document.getElementsByTagName('tbody')[1];
	
	// clear my tbody
	tbody.innerHTML = "";
	// add new content
	for (i = 0; i < arrayFavContacts.length; i++) { 
		row += "<tr id='montr_" + i + "'>";
			row += "<td>" + arrayFavContacts[i].knom + "</td>";
			row += "<td>" + arrayFavContacts[i].kprenom + "</td>";
			row += "<td>" + arrayFavContacts[i].ktel + "</td>";
			row += "<td>" + arrayFavContacts[i].kemail + "</td>";
			row += "<td>";
				row += "<button type='button' class='btn btn-danger btn-md' onclick='supprimerFavoris(" + i + ");'>";
					row += "Supprimer";
				row += "</button>";
			row += "</td>";
		row += "</tr>";
	}
	tbody.innerHTML = row;
	
	document.getElementById('nbrFavoris').innerHTML = arrayFavContacts.length;
	return false;
};
